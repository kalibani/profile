import styled, { createGlobalStyle } from "styled-components";

interface GlobalStyleProps {
  greyBackground?: boolean;
}

export const GlobalStyle = createGlobalStyle<GlobalStyleProps>`
  body {
    background: ${(greyBackground) => (greyBackground ? "#F5F5F5" : "black")};
    font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", "Roboto", "Oxygen",
    "Ubuntu", "Cantarell", "Fira Sans", "Droid Sans", "Helvetica Neue",
    sans-serif;
  }
`;

export const ContainerStyle = styled.div`
  min-height: 100vh;
  min-width: 100vw;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const WrapperForm = styled.div`
  min-width: 350px;
  min-height: 400px;
  background: #ffffff;
  margin: 0 auto;
  border-radius: 4px;
  padding: 20px 10px;
  padding: 20px 15px;
  display: flex;
  flex-direction: column;
  align-items: center;
  box-shadow: 0px 2px 16px 0px rgba(202, 211, 225, 0.4);
  -webkit-box-shadow: 0px 2px 16px 0px rgba(202, 211, 225, 0.4);
  -moz-box-shadow: 0px 2px 16px 0px rgba(202, 211, 225, 0.4);
`;
