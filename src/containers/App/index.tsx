import React, { useEffect, useState } from "react";
import { useForm, Controller, SubmitHandler } from "react-hook-form";
import Button from "components/ Button";
import TextField from "components/TextField";
import Avatar from "components/Avatar/index";
import { getProfile } from "API";

import { GlobalStyle, ContainerStyle, WrapperForm } from "./style";

type Inputs = {
  username: string;
  email: string;
  website: string;
};

const App: React.FC = () => {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const [profile, setProfile] = useState({
    username: "",
  });

  const fetchData = async (): Promise<void> => {
    const result = await getProfile();

    console.log(result.data);

    setProfile(result.data);
  };

  useEffect(() => {
    fetchData();
  }, []);

  const {
    handleSubmit,
    control,
    formState: { errors },
  } = useForm<Inputs>();
  const onSubmit: SubmitHandler<Inputs> = (data) => console.log(data);

  return (
    <>
      <GlobalStyle greyBackground />
      <ContainerStyle>
        <WrapperForm>
          <Avatar
            handleChangePicture={() => {
              console.warn("test");
            }}
          />
          <form onSubmit={handleSubmit(onSubmit)}>
            <Controller
              name="username"
              control={control}
              rules={{ required: true }}
              render={({ field }) => <TextField {...field} label="Username" />}
            />
            {errors.username && <span>This field is required</span>}

            <Controller
              name="email"
              control={control}
              rules={{
                required: "Please enter a value",
                pattern: {
                  value: /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/,
                  message: "An email address must contain a single @",
                },
              }}
              render={({ field }) => <TextField {...field} label="Email*" />}
            />
            {errors.email && <span>{errors.email.message}</span>}

            <Controller
              name="website"
              control={control}
              rules={{
                pattern: {
                  value:
                    /(:?(?:https?:\/\/)?(?:www\.)?)?[-a-z0-9]+\.(?:com|gov|org|net|edu|biz)/g,
                  message: "Please input a valid website URL",
                },
              }}
              render={({ field }) => <TextField {...field} label="Website" />}
            />
            {errors.website && <span>{errors.website.message}</span>}

            <Button type="submit" look="dark" size="medium">
              Submit
            </Button>
          </form>
        </WrapperForm>
      </ContainerStyle>
    </>
  );
};

export default App;
