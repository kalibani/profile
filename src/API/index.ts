import axios from "axios";

const baseURL: string = process.env.REACT_APP_BASE_URL as string;

export const getProfile = (): any => axios.get(`${baseURL}/profile`);
export const postProfile = (): any => axios.post(`${baseURL}/profile`);
