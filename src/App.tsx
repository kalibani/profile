import React, { useEffect, useState } from "react";
import { useForm, SubmitHandler } from "react-hook-form";
import Button from "./components/ Button";
import { getProfile } from "./API";

import "./App.css";

type Inputs = {
  username: string;
  email: string;
};

const App: React.FC = () => {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const [profile, setProfile] = useState();

  const fetchData = async (): Promise<void> => {
    const result = await getProfile();

    console.log(result.data);

    setProfile(result.data);
  };

  useEffect(() => {
    fetchData();
  }, []);

  const {
    register,
    handleSubmit,
    watch,
    formState: { errors },
  } = useForm<Inputs>();
  const onSubmit: SubmitHandler<Inputs> = (data) => console.log(data);

  console.log(watch("username")); // watch input value by passing the name of it

  return (
    /* "handleSubmit" will validate your inputs before invoking "onSubmit" */
    <form onSubmit={handleSubmit(onSubmit)}>
      {/* register your input into the hook by invoking the "register" function */}

      <input {...register("username", { required: true })} />
      {/* include validation with required or other standard HTML validation rules */}
      <input {...register("email", { required: true })} />
      {/* errors will return when field validation fails  */}
      {errors.email && <span>This field is required</span>}

      <Button type="submit" look="light">
        Submit
      </Button>
    </form>
  );
};

export default App;
