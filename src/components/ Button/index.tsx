import * as React from "react";
import { StyledButton } from "./style";

interface ButtonProps {
  children?: React.ReactChild;
  className?: string;
  radius?: number;
  size?: "small" | "medium" | "big";
  look?: "primary" | "secondary" | "dark" | "light";
  type?: "button" | "submit";
}

const Button: React.FC<ButtonProps> = ({
  className,
  children,
  radius,
  size,
  look,
  type,
}) => (
  <StyledButton
    type={type}
    className={className}
    radius={radius}
    size={size}
    look={look}
  >
    {children}
  </StyledButton>
);

export default Button;
