import styled from "styled-components";

export const Container = styled.div`
  height: 170px;
  width: 170px;
  position: relative;
  border-radius: 50%;
  /* border: 1px solid #d5d1d1; */
  box-shadow: 0px 2px 16px 0px rgba(202, 211, 225, 0.4);
  -webkit-box-shadow: 0px 2px 16px 0px rgba(202, 211, 225, 0.4);
  -moz-box-shadow: 0px 2px 16px 0px rgba(202, 211, 225, 0.4);
`;
// https://cdn.iconscout.com/icon/free/png-512/edit-2653398-2202588.png

export const IconWrapper = styled.div`
  height: 30px;
  width: 30px;
  position: absolute;
  bottom: 0;
  right: 15px;
  background-color: white;
  border-radius: 40%;
  box-shadow: 0px 2px 16px 0px rgba(202, 211, 225, 0.4);
  -webkit-box-shadow: 0px 2px 16px 0px rgba(202, 211, 225, 0.4);
  -moz-box-shadow: 0px 2px 16px 0px rgba(202, 211, 225, 0.4);
  background-color: #273444;
  display: grid;
  place-items: center;
`;

export const IconEdit = styled.img`
  height: 20px;
  border-radius: 50%;
`;

export const AvatarDisplay = styled.img`
  height: 100%;
  width: 100%;
  border-radius: 50%;
`;

export const Label = styled.label`
  cursor: pointer;
`;
