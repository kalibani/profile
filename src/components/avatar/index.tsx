import React, { useState } from "react";
import { toBase64 } from "helper";
// eslint-disable-next-line object-curly-newline
import iconImage from "assets/images/editIcon.png";
import {
  Container,
  IconEdit,
  AvatarDisplay,
  Label,
  IconWrapper,
} from "./style";

type AvatarType = {
  handleChangePicture: (base64: string) => void;
};

const Avatar: React.FC<AvatarType> = ({ handleChangePicture }) => {
  const [avatarPic, setAvatarPic] = useState(
    "https://sunrift.com/wp-content/uploads/2014/12/Blake-profile-photo-square.jpg"
  );
  const handleChangeAvatar = (e: React.ChangeEvent<HTMLInputElement>): any => {
    const {
      target: { files },
    } = e;
    toBase64(files![0]).then((base64: any) => {
      setAvatarPic(base64);
      handleChangePicture(base64);
    });
  };
  return (
    <Container>
      <AvatarDisplay src={avatarPic} />
      <Label htmlFor="avatar-input">
        <IconWrapper>
          <IconEdit src={iconImage} alt="edit" />
        </IconWrapper>
      </Label>
      <input
        id="avatar-input"
        type="file"
        hidden
        accept="image/*"
        name="avatar"
        onChange={handleChangeAvatar}
      />
    </Container>
  );
};

export default Avatar;
