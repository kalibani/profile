import React from "react";
import { StyledTextFiled, InputContainer, Label } from "./style";

type TypePropsInput = {
  label?: string;
  value?: string | number;
  type?: string;
  name?: string;
  onChange?: () => void;
  required?: boolean;
};

const TextField: React.FC<TypePropsInput> = ({
  label,
  value,
  type,
  onChange,
  ...rest
}) => (
  <InputContainer>
    <Label>{label}</Label>
    <StyledTextFiled value={value} type={type} onChange={onChange} {...rest} />
  </InputContainer>
);

export default TextField;
