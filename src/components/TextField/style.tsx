import styled from "styled-components";

type TypePropsInput = {
  value: string | number;
  type: string;
  onChange?: () => void;
};

export const StyledTextFiled = styled.input.attrs(
  ({ onChange, value, type }: TypePropsInput) => ({
    onChange,
    value,
    type,
  })
)<TypePropsInput>`
  display: block;
  width: -webkit-fill-available;
  border-radius: 4px;
  border: 1px solid #d5d1d1;
  font-size: 14px;
  padding: 10px 15px;
  :focus {
    outline: none;
  }
`;
export const InputContainer = styled.div`
  margin-bottom: 20px;
  margin-top: 10px;
  width: 100%;
`;

export const Label = styled.label`
  line-height: 2;
  text-align: left;
  display: block;
  margin-bottom: 5px;
  font-size: 1rem;
`;
